Summary: AlmaLinux devel repository configuration
Name:    almalinux-release-devel
Version: 9
Release: 1%{?dist}
License: GPLv2
URL:     https://almalinux.org/
BuildArch: noarch
Source0: almalinux-devel.repo

Provides: almalinux-release-devel = 9

%description
DNF configuration for AlmaLinux devel repo
Not for production. For buildroot use only

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/almalinux-devel.repo

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/almalinux-devel.repo

%changelog
* Mon Aug 14 2023 Andrew Lukoshko <alukoshko@almalinux.org> - 9-1
- Initial version
